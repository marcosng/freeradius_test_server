## FreeRADIUS Server

Este repositorio contiene la información y archivos necesarios para desplegar un servidor RADIUS dockerizado que interactúe con CeraOptimizer Backhaul.

---

### Contenido:
_default_: modifica la respuesta standard del servidor radius añadiendo la clave "User-Name" (linea 804)

_raddb/clients.conf_: define los clientes que podrán realizar consulta al servidor radius.

_raddb/mods-config/files/authorize_: define usuario/contraseña autorizados.

_build.sh_: script para construir la imagen docker

---
### Generando la imagen:
1. Editar el archivo _clients.conf_ configurando "secret" y la dirección IP del servidor.

2. Editar el archivo _authorize_ de acuerdo a los usuarios habilitados.

3. Generar la imagen Docker mediante _build.sh_

---
### Una primera prueba:
1. Una vez con la imagen en el sistema, lanzar un contenedor externalizando los puertos necesarios:
> docker run -p 1812-1813:1812-1813/udp radius_test_server -X


2. Utilizar _radtest_ para enviar una consulta de prueba:
> radtest <user_name> <pass_word> <server_ip_address> 0 <secret_key>
   

3. Leemos la respuesta:   

Ejemplo:  

``` 
radtest guest2 test 192.168.0.119 0 C14r0.OP

Sent Access-Request Id 252 from 0.0.0.0:53748 to 192.168.0.119:1812 length 76  
	User-Name = "guest2"  
	User-Password = "test"  
	NAS-IP-Address = 127.0.1.1  
	NAS-Port = 0  
	Message-Authenticator = 0x00  
	Cleartext-Password = "test"  
Received Access-Accept Id 252 from 192.168.0.119:1812 to 192.168.0.119:53748 length 39  
	Reply-Message = "full_user"  
	User-Name = "guest2"
```

---
### Configurando Django:
Las siguientes variables deben estar presentes en el contenedor Django.  
Tener en cuenta de reemplazar RADIUS_SERVER por la dirección de IP de la PC donde se desplegará el freeradius.  

```
-
RADIUS_SERVER=192.168.0.119  
RADIUS_SECRET=C14r0.OP  
RADIUS_PORT=1812  
RADIUS_DEFAULT_GROUP=read only user  
RADIUS_AUTH_ENABLE=True  
RADIUS_USER_GROUP_MAPPING={"full_user":"full user","read_user":"read only user"}   
```
---
Testeado con freeradius/freeradius-server:3.0.21 y unclcd/backhaul:0.1.235  
Para mas info ve ticket: https://unclcd.atlassian.net/browse/GEN-102