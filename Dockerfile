FROM freeradius/freeradius-server:latest
COPY raddb/ /etc/raddb/
COPY default /etc/raddb/sites-enabled